# HammerStruggle

What have I learned in Unreal Engine?
  - Blueprints
  - Visual scripting
  - Animation as FSM
  - UI
  - Game logic
  - Collision
  - Behavior trees

This submission was a group project. We had 3 months from the idea to the finished game.
There were 4 of us in this group: 2 programmers and 2 artists. Unfortunately one member of the group, who was a programmer, fell ill shortly after the alpha version was finished and so I had to do most of the programming myself. Despite this, all the necessary features were implemented.

**How does it work in the Background**

Resources:
The player has an array of structs where the quantity of the respective resource is also stored. The resource has the index as a variable where it must be added to the player's array.
  - Wood: 0
  - Stone: 1
  - Iron: 2
  - Copper: 3
  - Marbel: 4
  - Silver: 5
  - Gem: 6
  - Gold: 7
The sprite and the description for the UI bindings are also saved in this structure. So that everything is central to the player when changes are made.

AI:
There are two types of AI. One that can perform melee attacks and one that can also throw. Each of these types has its own behavior tree and animation FSM, but both have the same father blueprint. So I can adjust the appearance and the behavior when dead for both at the same time in the blueprint.

Player animation:
With the player animation, the required animation is selected via an enum. The upper body plays the attack animation and the lower body continues to follow the run or idle animation, which are faded.
Switching weapons from the back to the hand is done via sockets.

Damage:
Damage is triggered via the damage function implemented by Unreal Engine. A ShpereTrace is made in the animation. If this hits, damage is dealt to the person hit.

Player Upgrades:
The costs for player upgrades are calculated using transform functions. The functions are implemented in such a way that the transformation can be adjusted flexibly. 

Well:
The well is unlocked as soon as the player has collected the first rare resource. There he has the option of charging the crystal, which then strengthens a nearby golem and spawns a specific resource. I have put the fountain in the forge with the trigger to open the UI, as well as the lamp in a blueprint to make it easier to reference the lamp.

Golem Spawner:
The number, HP, damage and resource dropped when the golem dies depends on the number of upgrades the player has made, as well as the number of turns. The values are calculated using a formula, which can also be flexibly adapted by the developers. 

Round Controller:
The Round Controller decides when a round ends and when a round continues. It has references to the doors and the golem spawners. When the player leaves the forge, the next round begins, the forge closes and the Spanwer spawns the golems. As soon as the golems are defeated, it closes the doors of the spawn area if the player is not inside. And opens the doors of the forge. Etc.

-----------------------------------------------------

Was habe ich in Unreal Engine 4 gelernt:
  - Blueprints
  - Visual Scripting
  - Animation as FSM
  - UI
  - Game Logic
  - Collision
  - Behaviour Trees

Diese Abgabe ist ein Gruppenprojekt gewesen. Wir hatten 3 Monate Zeit, von der Idee bis zum fertigen Spiel.
Wir waren zu 4 in dieser Gruppe: 2 Programmierer und 2 Artist. Leider ist ein Gruppenmitglied, welcher Programmierer war, kurz nach der Fertigstellung der Alpha Version krank geworden und somit musste das meiste von mir alleine programmiert werden. Trotz dessen wurden alle nötigen Features implementiert.

**Wie es im Hintergrund funktioniert**

Ressourcen:
Der Spieler besitzt ein Array aus Structs wo auch die Menge der jeweiligen Ressource gespeichert wird. Die Ressource besitzt als Variabel den Index, wo sie im Array beim Spieler hinzugefügt werden muss.
  - Wood: 0
  - Stone: 1
  - Iron: 2
  - Copper: 3
  - Marbel: 4
  - Silver: 5
  - Gem: 6
  - Gold: 7
In diesem Struct wird auch das Sprite als auch die Beschreibung für die UI-Bindings gespeichert. Damit bei Änderungen alles Zentral beim Spieler ist.

AI:
Es gibt zwei Arten von AI. Eine die Nahkampfangriffe ausführen kann und eine die noch zusätzlich werfen kann. Jede dieser Art besitzt einen eigenen Behaviour Tree und Animations-FSM, aber beide besitzen denselben Vater-Blueprint. So kann ich im Blueprint das Aussehen und das Verhalten beim Tot für beide gleichzeitig anpassen.

Player Animation:
Bei der Playeranimation wird über ein Enum die benötigte Animation ausgewählt. Dabei spielt der Oberkörper die Attack-Animation ab und der Unterkörper folgt weiter der Lauf bzw. der Idle Animation, welche geblendet werden, weiter.
Das wechseln der Waffen von dem Rücken in die Hand wird über Sockets gemacht.

Damage:
Der Schaden wird über die von Unreal Engine implementierte Damage Funktion ausgelöst. Dabei wird in der Animation ein ShpereTrace gemacht. Wenn dieser Trifft wird dem Getroffenen Schaden erteilt.

Player Upgrades:
Die Kosten für die Player Upgrades werden durch Transformiere Funktionen berechnet. Die Funktionen sind so implementiert, dass man flexibel die Transformation anpassen könnte. 

Brunnen:
Der Brunnen wird freigeschaltet sobald der Spieler die erste seltene Ressource eingesammelt hat. Dort hat er die Möglichkeit den Kristall aufzuladen, welcher dann einen Golem in der Nähe verstärkt und eine bestimmte Ressource spawnen lässt. Ich habe den Brunnen in der Schmiede mit dem Trigger um die UI zu öffnen, als auch die Lampe in ein Blueprint gepackt umso einfachere auf die Lampe zu referenzieren.

Golem Spawner:
Die Anzahl, die HP, der Schaden und die Ressource, die beim Sterben gedropt wird, der Golems ist abhängig von der Anzahl an Upgrades die der Spieler gemacht hat, als auch von der Rundenanzahl. Die Werte werden über eine Formel errechnet, welche auch von den Entwicklern flexibel angepasst werden kann. 

Round Controller:
Der Round Controller entscheidet, wann eine Runde endet und wann eine Runde weiter geht. Dieser besitzt Referenzen zu den Türen und zu dem Golem Spawner. Wenn der Spieler die Schmieder verlässt, beginnt die nächste Runde, die Schmiede schließt sich und der Spanwer spawnt die Golems. Sobald die Golems besiegt sind, schließt dieser die Türen des Spawnbereiches, wenn sich er Spieler nicht drinnen befindet. Und öffnet die Türen der Schmiede. Usw.